package dev.arifisme.salttest.consumer;

import dev.arifisme.salttest.utils.Formatter;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class ConsumerDTOMapper implements Function<Consumer, ConsumerDTO> {
    @Override
    public ConsumerDTO apply(Consumer consumer) {
        return new ConsumerDTO(
                consumer.getId(),
                consumer.getName(),
                consumer.getAddress(),
                consumer.getCity(),
                consumer.getProvince(),
                Formatter.dateTime(consumer.getRegisteredDate()),
                consumer.getStatus() == ConsumerStatus.ACTIVE
        );
    }
}
