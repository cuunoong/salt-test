package dev.arifisme.salttest.consumer;

public record ConsumerDTO (
        Integer id,
        String name,
        String address,
        String city,
        String province,
        String registeredDate,
        boolean isActive
){
}
