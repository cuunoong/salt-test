package dev.arifisme.salttest;

import dev.arifisme.salttest.consumer.Consumer;
import dev.arifisme.salttest.consumer.ConsumerRepository;
import dev.arifisme.salttest.consumer.ConsumerStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DatabaseSeeder implements CommandLineRunner {

    private final ConsumerRepository consumerRepository;

    @Override
    public void run(String... args) {
        consumerRepository.saveAll(List.of(
                Consumer
                        .builder()
                        .name("Arif Iskandar")
                        .address("Jl. Kemuning, Gg. Kemuning Dalam, No. 2A")
                        .city("Medan")
                        .province("Sumatera Utara")
                        .registeredDate(LocalDateTime.now())
                        .status(ConsumerStatus.ACTIVE)
                        .build()
        ));
    }
}
