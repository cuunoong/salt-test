package dev.arifisme.salttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaltIndonesiaJavaDeveloperTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaltIndonesiaJavaDeveloperTestApplication.class, args);
	}

}
