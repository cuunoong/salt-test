package dev.arifisme.salttest.consumer;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Table
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Consumer {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String address;
    private String city;
    private String province;
    private LocalDateTime registeredDate;
    @Enumerated(EnumType.STRING)
    private ConsumerStatus status;

}
