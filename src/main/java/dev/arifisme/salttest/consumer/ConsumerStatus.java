package dev.arifisme.salttest.consumer;

public enum ConsumerStatus {
    ACTIVE,
    NON_ACTIVE
}
