<%@ page import="dev.arifisme.salttest.consumer.ConsumerDTO" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detail Konsumen</title>
    <%-- Bootstrap style  --%>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <%-- Datatable style --%>
    <link href="https://cdn.datatables.net/1.13.2/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>

<%-- Main --%>
<main class="container">
    <div class="pt-3 d-flex justify-content-between">
        <div>
            <span class="text-muted fw-normal mt-0">Detail</span>
            <h4 class="text-body">Konsumen</h4>
        </div>
        <div class="d-flex ">
            <a href="/consumer" class="btn btn-light me-md-2 align-self-center" type="button">Kembali</a>
        </div>
    </div>
    <hr/>
    <%
        ConsumerDTO consumer = (ConsumerDTO) request.getAttribute("consumer");
    %>
    <form action="/consumer/<%=consumer.id()%>/update" method="post">
        <div class="mb-3">
            <label for="name" class="form-label">Nama Lengkap</label>
            <input type="text" name="name" class="form-control" id="name" value="<%=consumer.name()%>">
        </div>
        <div class="mb-3">
            <label for="address" class="form-label">Alamat</label>
            <input type="text" name="address" class="form-control" id="address"  value="<%=consumer.address()%>">
        </div>
        <div class="mb-3">
            <label for="city" class="form-label">Kota</label>
            <input type="text" name="city" class="form-control" id="city"  value="<%=consumer.city()%>">
        </div>
        <div class="mb-3">
            <label for="province" class="form-label">Province</label>
            <input type="text" name="province" class="form-control" id="province"  value="<%=consumer.province()%>">
        </div>
        <div class="d-flex">
            <button type="reset" class="btn btn-light me-2">Reset</button>
            <button type="submit" class="btn btn-primary">Simpan perubahan</button>
        </div>
    </form>
</main>
</body>
</html>