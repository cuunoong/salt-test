package dev.arifisme.salttest.consumer;

public record ConsumerRequest(
        String name,
        String address,
        String city,
        String province
) {
}
