package dev.arifisme.salttest.consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequiredArgsConstructor
@RequestMapping("/consumer") @Slf4j
public class ConsumerController {

    private final ConsumerService consumerService;

    @GetMapping
    public  String index(Model model) {
        return  consumerService.showAllConsumers(model);
    }

    @GetMapping("create")
    public String create() {
        return "create";
    }

    @PostMapping( "store")
    public RedirectView store(@ModelAttribute ConsumerRequest request) {
        return consumerService.createNewConsumer(request);
    }

    @GetMapping("{consumerId}")
    public String show(@PathVariable("consumerId") int consumerId,  Model model) {
        return consumerService.showConsumer(consumerId, model);
    }

    @PostMapping("{consumerId}/update")
    public RedirectView update(@PathVariable("consumerId") int consumerId, @ModelAttribute ConsumerRequest request) {
        return consumerService.updateConsumer(consumerId, request);
    }

    @PostMapping("{consumerId}/activate")
    public RedirectView activate(@PathVariable("consumerId") int consumerId) {
        return consumerService.activateConsumer(consumerId);
    }

    @PostMapping("{consumerId}/delete")
    public RedirectView delete(@PathVariable("consumerId") int consumerId) {
        return consumerService.deleteConsumer(consumerId);
    }
}
