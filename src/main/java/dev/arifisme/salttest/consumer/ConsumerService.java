package dev.arifisme.salttest.consumer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsumerService {
    private final ConsumerRepository consumerRepository;
    private final ConsumerDTOMapper consumerDTOMapper;

    public String showAllConsumers(Model model) {
        List<ConsumerDTO> consumers = consumerRepository.findAll().stream().map(consumerDTOMapper).toList();

        model.addAttribute("consumers", consumers);
        return "index";
    }

    public RedirectView createNewConsumer(ConsumerRequest request) {
        Consumer consumer = Consumer.builder()
                .name(request.name())
                .address(request.address())
                .city(request.city())
                .province(request.province())
                .registeredDate(LocalDateTime.now())
                .status(ConsumerStatus.NON_ACTIVE)
                .build();

        consumerRepository.save(consumer);

        return new RedirectView("/consumer");
    }

    public String showConsumer(int consumerId, Model model) {
        ConsumerDTO consumerDTO = consumerRepository.findById(consumerId).map(consumerDTOMapper)
                .orElseThrow(() -> new IllegalStateException("Not found"));

        model.addAttribute("consumer", consumerDTO);

        return "show";
    }

    @Transactional
    public RedirectView updateConsumer(int consumerId, ConsumerRequest request) {
        Consumer consumer = consumerRepository.findById(consumerId)
                .orElseThrow(() -> new IllegalStateException("Not found"));

        consumer.setName(request.name());
        consumer.setAddress(request.address());
        consumer.setCity(request.city());
        consumer.setProvince(request.province());

        return new RedirectView("/consumer");
    }

    @Transactional
    public RedirectView activateConsumer(int consumerId) {
        Consumer consumer = consumerRepository.findById(consumerId)
                .orElseThrow(() -> new IllegalStateException("Not found"));
        consumer.setStatus(ConsumerStatus.ACTIVE);

        return new RedirectView("/consumer");
    }

    @Transactional
    public RedirectView deleteConsumer(int consumerId) {
        consumerRepository.deleteById(consumerId);

        return new RedirectView("/consumer");
    }
}
